﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace SharpManager
{
    public partial class MainWindow : Window
    {
        private Functions _functions;
        private bool _operationOngoing = false;

        private bool OperationOngoing
        {
            get { return _operationOngoing; }
            set
            {
                _operationOngoing = value;

                this.Dispatcher.Invoke(delegate
                {
                    SetButtonsEnabled(!value);
                    this.Title = "SharpManager " + (value ? "(Ongoing..)" : "");
                });
            }
        }

        private Dictionary<CheckBox, Project> _projectChks = new Dictionary<CheckBox, Project>();
        private IEnumerable<Project> CheckedProjects => Dispatcher.Invoke(() => _projectChks.Where(p => p.Key.IsChecked == true).Select(p => p.Value));

        private Dictionary<CheckBox, PublishFile> _publishFileChks = new Dictionary<CheckBox, PublishFile>();
        private IEnumerable<PublishFile> CheckedPublishFiles => Dispatcher.Invoke(() => _publishFileChks.Where(p => p.Key.IsChecked == true).Select(p => p.Value));

        public MainWindow()
        {
            InitializeComponent();

            _functions = new Functions(this.Console);

            foreach (var project in ProjectDictionary.Projects)
            {
                var checkbox = new CheckBox
                {
                    Content = project.Name,
                    IsChecked = true
                };
                stackPanelCheckboxes.Children.Add(checkbox);
                _projectChks.Add(checkbox, project);
            }

            foreach (var publishFile in ProjectDictionary.PublishFiles)
            {
                var checkbox = new CheckBox
                {
                    Content = Path.GetFileName(publishFile.Source),
                    IsChecked = true
                };
                stackPanelCheckboxes.Children.Add(checkbox);
                _publishFileChks.Add(checkbox, publishFile);
            }
        }

        private void SetButtonsEnabled(bool enabled)
        {
            BtnBuild.IsEnabled = enabled;
            BtnDoAll.IsEnabled = enabled;
            BtnCopy.IsEnabled = enabled;
            BtnPublish.IsEnabled = enabled;
            BtnSelectAll.IsEnabled = enabled;
            BtnDeselectAll.IsEnabled = enabled;

            foreach (var chkProject in _projectChks.Keys)
            {
                chkProject.IsEnabled = enabled;
            }

            foreach (var chkProject in _publishFileChks.Keys)
            {
                chkProject.IsEnabled = enabled;
            }
        }

        private void BtnBuild_Click(object sender, RoutedEventArgs e)
        {
            var projects = CheckedProjects.Select(p => p).ToList();

            Thread t = new Thread((ThreadStart)delegate
           {
               OperationOngoing = true;
               _functions.Build(projects);
               OperationOngoing = false;
           });
            t.IsBackground = true;
            t.Start();
        }

        private void BtnCopy_Click(object sender, RoutedEventArgs e)
        {
            var projects = CheckedProjects.Select(p => p).ToList();

            Thread t = new Thread((ThreadStart)delegate
           {
               OperationOngoing = true;
               _functions.Copy(projects);
               OperationOngoing = false;
           });
            t.IsBackground = true;
            t.Start();
        }

        private void BtnPublish_Click(object sender, RoutedEventArgs e)
        {
            var projects = CheckedProjects.Select(p => p).ToList();
            var publishFiles = CheckedPublishFiles.Select(p => p).ToList();

            Thread t = new Thread((ThreadStart)delegate
           {
               OperationOngoing = true;
               _functions.Publish(projects, publishFiles);
               OperationOngoing = false;
           });
            t.IsBackground = true;
            t.Start();
        }

        private void BtnDoAll_Click(object sender, RoutedEventArgs e)
        {
            var projects = CheckedProjects.Select(p => p).ToList();
            var publishFiles = CheckedPublishFiles.Select(p => p).ToList();

            Thread t = new Thread((ThreadStart)delegate
           {
               OperationOngoing = true;
               _functions.Build(projects);
               Thread.Sleep(1000);
               _functions.Copy(projects);
               Thread.Sleep(1000);
               _functions.Publish(projects, publishFiles);
               OperationOngoing = false;
           });
            t.IsBackground = true;
            t.Start();
        }

        private void MenuItemCredts_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start("http://mariusfreitag.de");
        }

        private void MenuItemInformation_OnClick(object sender, RoutedEventArgs e)
        {
            new InfoWindow().ShowDialog();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (OperationOngoing)
            {
                if ((MessageBox.Show("SharpManager is still operating, do you really want to exit?", "",
                         MessageBoxButton.YesNo) == MessageBoxResult.No))
                {
                    e.Cancel = true;
                }
            }
        }

        private void BtnSelectAll_Click(object sender, RoutedEventArgs e)
        {
            foreach (var checkbox in _projectChks.Keys)
            {
                checkbox.IsChecked = true;
            }

            foreach (var checkbox in _publishFileChks.Keys)
            {
                checkbox.IsChecked = true;
            }
        }

        private void BtnDeselectAll_Click(object sender, RoutedEventArgs e)
        {
            foreach (var checkbox in _projectChks.Keys)
            {
                checkbox.IsChecked = false;
            }

            foreach (var checkbox in _publishFileChks.Keys)
            {
                checkbox.IsChecked = false;
            }
        }
    }
}