﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace SharpManager
{
    public class Functions
    {
        public ConsoleControl Console;

        public Functions(ConsoleControl console)
        {
            this.Console = console;
        }

        public void Build(IEnumerable<Project> projects)
        {
            Console.WriteHeading("Builder");

            foreach (var project in projects)
            {
                Console.WriteSubheading(project.Name);
                Process buildProcess = new Process
                {
                    StartInfo = new ProcessStartInfo()
                    {
                        FileName = ProjectDictionary.MsBuildPath,
                        Arguments =
                            $"\"{project.SolutionPath}\" /p:Configuration=Release /nologo /verbosity:minimal /p:Platform=\"Any CPU\"",
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                        CreateNoWindow = true
                    }
                };
                buildProcess.Start();

                while (!buildProcess.StandardOutput.EndOfStream)
                {
                    Console.WriteLine(buildProcess.StandardOutput.ReadLine());
                }

                while (!buildProcess.StandardError.EndOfStream)
                {
                    Console.WriteLine(buildProcess.StandardError.ReadLine());
                }

                int error = buildProcess.ExitCode;

                if (error == 1)
                {
                    Console.WriteFailureLine("Error at " + project.Name + "!");
                    Console.WaitForConfirmation();
                }
            }

            Console.WriteSuccessLine("Builder finished");
        }

        public void Copy(IEnumerable<Project> projects)
        {
            Console.WriteHeading("Copier");

            if (!Directory.Exists(ProjectDictionary.OutPath))
            {
                Directory.CreateDirectory(ProjectDictionary.OutPath);
            }

            foreach (var project in projects)
            {
                var destinationPath = Path.Combine(ProjectDictionary.OutPath, Path.GetFileName(project.BinaryPath));
                try
                {
                    File.Copy(project.BinaryPath, destinationPath, true);
                    Console.WriteLine("File copied: " + destinationPath);
                }
                catch
                {
                    Console.WriteErrorLine($"Error at {project.Name}! ({destinationPath})");
                }
            }

            Console.WriteSuccessLine("Copier finished");
        }

        public void Publish(IEnumerable<Project> projects, IEnumerable<PublishFile> files)
        {
            Console.WriteHeading("Publisher");

            foreach (var project in projects)
            {
                foreach (var publishPath in project.PublishPaths)
                {
                    try
                    {
                        if (!Directory.Exists(Path.GetDirectoryName(publishPath)))
                        {
                            Directory.CreateDirectory(Path.GetDirectoryName(publishPath));
                        }

                        File.Copy(
                            ProjectDictionary.OutPath + "\\" + project.Name +
                            project.BinaryPath.Substring(project.BinaryPath.Length - 4), publishPath, true);
                        Console.WriteLine("File published: " + publishPath);
                    }
                    catch (Exception)
                    {
                        Console.WriteErrorLine($"Error at {project.Name}! ({publishPath})");
                    }
                }
            }

            foreach (var file in files)
            {
                foreach (var destination in file.Destinations)
                {
                    try
                    {
                        if (!Directory.Exists(Path.GetDirectoryName(destination)))
                        {
                            Directory.CreateDirectory(Path.GetDirectoryName(destination));
                        }

                        File.Copy(file.Source, destination, true);
                        Console.WriteLine("File published: " + destination);
                    }
                    catch (Exception)
                    {
                        Console.WriteErrorLine($"Error at {destination}!");
                    }
                }

            }

            Console.WriteSuccessLine("Publisher finished");
        }
    }
}