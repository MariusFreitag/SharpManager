﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SharpManager
{
    public static class ProjectDictionary
    {
        public static readonly string MsBuildPath;
        public static readonly string OutPath;
        public static readonly string ProjectRootFolder;
        public static readonly List<Project> Projects;
        public static readonly List<PublishFile> PublishFiles;

        static ProjectDictionary()
        {
            MsBuildPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), "Microsoft Visual Studio", "2017", "Community", "MSBuild", "15.0", "Bin", "MSBuild.exe");
            OutPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "Applications");
            ProjectRootFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "Projects");

            Projects = new List<Project>
            {
                new Project("SharpManager", ProjectRootFolder)
            };

            PublishFiles = new List<PublishFile>()
            {
            };
        }
    }

    public class Project
    {
        public string Name { get; set; }
        public string SolutionPath { get; set; }
        public string BinaryPath { get; set; }
        public List<string> PublishPaths { get; set; }

        public Project(string name, string projectRootFolder)
        {
            Name = name;
            SolutionPath = Path.Combine(projectRootFolder, Name, Name + ".sln");
            BinaryPath = Path.Combine(projectRootFolder, Name, Name, "bin", "Release", Name + ".exe");
            PublishPaths = new List<string>();
        }
    }

    public class PublishFile
    {
        public string Source { get; set; }
        public List<string> Destinations { get; set; }
    }
}