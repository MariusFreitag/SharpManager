﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SharpManager
{
    public partial class InfoWindow : Window
    {
        public InfoWindow()
        {
            InitializeComponent();

            PrintProjectDictionary();
        }

        private void PrintProjectDictionary()
        {
            TxtInfo.Text += "\n\n\n";
            TxtInfo.Text += "MsBuild-Path: \t\t" + ProjectDictionary.MsBuildPath;
            TxtInfo.Text += "\n" + "DefaultOutput-Path: \t" + ProjectDictionary.OutPath;

            TxtInfo.Text += "\n\n\n" + "HARDCODED PROJECTS:";

            foreach (var project in ProjectDictionary.Projects)
            {
                TxtInfo.Text += "\n\n" + project.Name + ":";
                TxtInfo.Text += "\n\t" + "BinaryPath: ";
                TxtInfo.Text += project.BinaryPath;
                TxtInfo.Text += "\n\t" + "SolutionPath: ";
                TxtInfo.Text += project.SolutionPath;
                TxtInfo.Text += "\n\t" + "PublishPaths: ";
                foreach (var publishPath in project.PublishPaths)
                {
                    TxtInfo.Text += "\n\t\t" + publishPath;
                }
            }

            TxtInfo.Text += "\n\n\n" + "HARDCODED PUBLISH FILES:";

            foreach (var file in ProjectDictionary.PublishFiles)
            {
                TxtInfo.Text += "\n\n" + file.Source + ":";
                TxtInfo.Text += "\n\t" + "Destinations: ";
                foreach (var destination in file.Destinations)
                {
                    TxtInfo.Text += "\n\t\t" + destination;
                }
            }
        }
    }
}