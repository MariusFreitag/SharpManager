﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace SharpManager
{
    public partial class ConsoleControl : UserControl
    {
        private const double DefaultTextSize = 12;
        private bool _confirmed = false;

        public ConsoleControl()
        {
            InitializeComponent();
        }

        public Dictionary<string, Template> GetDefaultTemplates()
        {
            return new Dictionary<string, Template>
            {
                { "Normal", new Template(Colors.Black, LineBreaks.Small, DefaultTextSize) },
                { "Error", new Template(Colors.Red, LineBreaks.Small, DefaultTextSize) },
                { "Heading", new Template(Colors.Black, LineBreaks.Big, DefaultTextSize + 3) },
                { "Subheading", new Template(Colors.Black, LineBreaks.Small, DefaultTextSize + 1) },
                { "Success", new Template(Colors.Green, LineBreaks.Big, DefaultTextSize + 3) },
                { "Failure", new Template(Colors.Red, LineBreaks.Big, DefaultTextSize + 3) }
            };
        }

        public void Print(Color color, double size, LineBreaks lineBreak, string text)
        {
            if (!Dispatcher.CheckAccess())
            {
                this.Dispatcher.Invoke(delegate
                {
                    Print(color, size, lineBreak, text);
                });
            }
            else
            {
                #region Modify text

                text = text.Replace("\n", Environment.NewLine);
                text = text.Replace("\r", Environment.NewLine);
                text = text.Replace("\r\n", Environment.NewLine);

                if (text.EndsWith(Environment.NewLine))
                {
                    text = text.Substring(0, text.Length - 1);
                }

                #endregion Modify text

                #region Add LineBreak

                string newLine = "";
                switch (lineBreak)
                {
                    case LineBreaks.None:
                        newLine = "";
                        break;

                    case LineBreaks.Small:
                        newLine = "\r";
                        break;

                    case LineBreaks.Big:
                        newLine = "\n";
                        break;

                    default:
                        newLine = "";
                        break;
                }

                #endregion Add LineBreak

                AppendToTxtProcess(color, size, text + newLine);
                TxtProcess.ScrollToEnd();
            }
        }

        public void PrintTemplate(string text, Template template)
        {
            Print(template.Color, template.Size, template.LineBreak, text);
        }

        public void Write(string text)
        {
            Template template = GetDefaultTemplates()["Normal"].Copy();
            template.LineBreak = LineBreaks.None;
            PrintTemplate(text, template);
        }

        public void WriteErrorLine(string text)
        {
            PrintTemplate(text, GetDefaultTemplates()["Error"]);
        }

        public void WriteFailureLine(string text)
        {
            PrintTemplate("", GetDefaultTemplates()["Heading"]);
            PrintTemplate(text, new Template(Colors.Red, LineBreaks.None, DefaultTextSize + 3));
            PrintTemplate("", GetDefaultTemplates()["Heading"]);
        }

        public void WriteHeading(string text)
        {
            PrintTemplate("", GetDefaultTemplates()["Heading"]);
            PrintTemplate(text, new Template(Colors.Black, LineBreaks.None, DefaultTextSize + 3));
            PrintTemplate("", GetDefaultTemplates()["Heading"]);
        }

        public void WriteLine(string text)
        {
            PrintTemplate(text, GetDefaultTemplates()["Normal"]);
        }

        public void WriteSubheading(string text)
        {
            PrintTemplate("", GetDefaultTemplates()["Normal"]);
            PrintTemplate(text, GetDefaultTemplates()["Subheading"]);
        }

        public void WriteSuccessLine(string text)
        {
            PrintTemplate("", GetDefaultTemplates()["Heading"]);
            PrintTemplate(text, new Template(Colors.Green, LineBreaks.None, DefaultTextSize + 3));
            PrintTemplate("", GetDefaultTemplates()["Heading"]);
        }

        private void AppendToTxtProcess(Color color, double size, string text)
        {
            Brush brush = new SolidColorBrush(color);
            TextRange range = new TextRange(TxtProcess.Document.ContentEnd, TxtProcess.Document.ContentEnd);
            range.Text = text;
            range.ApplyPropertyValue(TextElement.ForegroundProperty, brush);
            range.ApplyPropertyValue(TextElement.FontSizeProperty, size);
        }

        #region Wait

        public void WaitForConfirmation()
        {
            this.Dispatcher.Invoke(ShowBtnConfirm);

            bool confirmed = false;

            while (!confirmed)
            {
                this.Dispatcher.Invoke(delegate
                {
                    confirmed = this._confirmed;
                });
                Thread.Sleep(100);
            }
        }

        private void BtnConfirm_Click(object sender, RoutedEventArgs e)
        {
            HideBtnConfirm();
        }

        private void HideBtnConfirm()
        {
            BtnConfirm.Visibility = Visibility.Hidden;
            _confirmed = true;
        }

        private void ShowBtnConfirm()
        {
            BtnConfirm.Visibility = Visibility.Visible;
            _confirmed = false;
        }

        #endregion
    }

    public class Template
    {
        public Template(Color color, LineBreaks lineBreak, double size)
        {
            this.Color = color;
            this.LineBreak = lineBreak;
            this.Size = size;
        }

        public Color Color { get; set; }

        public LineBreaks LineBreak { get; set; }

        public double Size { get; set; }

        public Template Copy()
        {
            return new Template(Color, LineBreak, Size);
        }
    }

    public enum LineBreaks
    {
        None,
        Small,
        Big
    }
}